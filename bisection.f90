REAL FUNCTION f(x)
	REAL x
	f = sin(x) - (x * x)
END FUNCTION f

PROGRAM bisection
	REAL a, b, e, c, f

	a = -1.0
	b = .5
	e = 1.0e-4

	IF (f(a) * f(b) > 0) THEN
		WRITE (*, *) 'Incorrect input data'
		STOP
	END IF

	DO WHILE (b - a > e)
		c = (a + b) / 2
		IF (f(c) == 0) THEN
			EXIT
		END IF
		IF (f(a) * f(c) < 0) THEN
			b = c
		ELSE
			a = c
		ENDIF
	END DO
	WRITE(*, *) c
END PROGRAM bisection
